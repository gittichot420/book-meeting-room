// edit group
$("#modal-edit").on("show.bs.modal", function (event) {
	if ($(event.relatedTarget).data("id") != null) {
		$("#edit_id").val($(event.relatedTarget).data("id"));
	}
	if ($(event.relatedTarget).data("name") != null) {
		$("#edit_name").val($(event.relatedTarget).data("name"));
	}
});

// del
$("#modal-del").on("show.bs.modal", function (event) {
	if ($(event.relatedTarget).data("id") != null) {
		$("#del_id").val($(event.relatedTarget).data("id"));
	}
});

// edit Sub group
$("#edit-list").on("show.bs.modal", function (event) {
	var g_id = $(event.relatedTarget).data("group");
	var url = $(event.relatedTarget).data("url");
	$("#edit_url").attr("action", url);
	$("#group_list option[id='" + g_id + "']").attr("selected", "selected");
	if ($(event.relatedTarget).data("id") != null) {
		$("#edit_id").val($(event.relatedTarget).data("id"));
	}
	if ($(event.relatedTarget).data("name") != null) {
		$("#edit_name").val($(event.relatedTarget).data("name"));
	}
});

$("#modal-specifically").on("show.bs.modal", function (event) {
	$("#m_id").val($(event.relatedTarget).data("id"));
	$("#month").val($(event.relatedTarget).data("month"));
	$("#salary").val($(event.relatedTarget).data("salary"));
});

// del
$("#del-list").on("show.bs.modal", function (event) {
	var url = $(event.relatedTarget).data("url");
	$("#del_url").attr("action", url);
	if ($(event.relatedTarget).data("id") != null) {
		$("#del_id").val($(event.relatedTarget).data("id"));
	}
});

// edit category
$("#edit-cat").on("show.bs.modal", function (event) {
	var type_id = $(event.relatedTarget).data("type");
	$("#cat_type option[id='" + type_id + "']").prop("selected", "selected");
	if ($(event.relatedTarget).data("id") != null) {
		$("#edit_id").val($(event.relatedTarget).data("id"));
	}
	if ($(event.relatedTarget).data("name") != null) {
		$("#edit_name").val($(event.relatedTarget).data("name"));
	}
});

// remove option:selected
$("#edit-cat").on("hidden.bs.modal", function () {
	$("#cat_type option:selected").removeProp("selected");
});
