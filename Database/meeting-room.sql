-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: meeting-room
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_ci_sessions`
--

DROP TABLE IF EXISTS `tb_ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_ci_sessions` (
  `id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `timestamp` varchar(120) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_ci_sessions`
--

LOCK TABLES `tb_ci_sessions` WRITE;
/*!40000 ALTER TABLE `tb_ci_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_meeting_room`
--

DROP TABLE IF EXISTS `tb_meeting_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_meeting_room` (
  `meet_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `meet_title` varchar(255) DEFAULT NULL,
  `meet_unit` varchar(50) DEFAULT NULL,
  `meet_name` varchar(255) DEFAULT NULL,
  `meet_position` varchar(255) DEFAULT NULL,
  `meet_detail` longtext,
  `meet_tell` varchar(50) DEFAULT NULL,
  `meet_date_start` date DEFAULT NULL,
  `meet_time_start` time DEFAULT NULL,
  `meet_date_end` date DEFAULT NULL,
  `meet_time_end` time DEFAULT NULL,
  `meet_status` int(1) DEFAULT '0',
  PRIMARY KEY (`meet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_meeting_room`
--

LOCK TABLES `tb_meeting_room` WRITE;
/*!40000 ALTER TABLE `tb_meeting_room` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_meeting_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_rooms`
--

DROP TABLE IF EXISTS `tb_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_rooms` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_name` varchar(150) DEFAULT NULL,
  `room_detail` longtext,
  `room_image` varchar(50) DEFAULT NULL,
  `room_color` varchar(50) DEFAULT NULL,
  `room_color_opacity` varchar(50) DEFAULT NULL,
  `room_active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_rooms`
--

LOCK TABLES `tb_rooms` WRITE;
/*!40000 ALTER TABLE `tb_rooms` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_users`
--

DROP TABLE IF EXISTS `tb_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tb_users` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_user` varchar(50) DEFAULT NULL,
  `u_pass` varchar(50) DEFAULT NULL,
  `u_realpass` varchar(50) DEFAULT NULL,
  `u_prefix` varchar(50) DEFAULT NULL,
  `u_fname` varchar(100) DEFAULT NULL,
  `u_lname` varchar(100) DEFAULT NULL,
  `u_position` varchar(100) DEFAULT NULL,
  `u_active` int(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_users`
--

LOCK TABLES `tb_users` WRITE;
/*!40000 ALTER TABLE `tb_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-09-02  9:02:09
