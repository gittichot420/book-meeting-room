<!-- Content Wrapper. Contains page content -->
<style>
    /* #get_users td:nth-child(7),
th:nth-child(7) {
    display: none;
} */

    #meetDetail p {
        color: #34495e !important;
    }
</style>

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">

            </div>
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12 mt-4">
                    <!-- title -->
                    <h2 class="float-left txt-tilte-page"><i class="fas fa-table"></i> ห้องประชุม
                    </h2>
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>"
                            class="text-secondary txt-page">หน้าหลัก</a> > ห้องประชุม</span>
                </div>
            </div>
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <section class="col-md-12">
                    <!-- DIRECT CHAT -->
                    <div class="card mt-4">
                        <div class="card-header">
                            <div class="card-tools">
                                <ul class="nav nav-pills ml-auto">
                                    <li class="nav-item">
                                        <a class="btn btn-success btn-sm" href="<?= base_url('admin/rooms/add'); ?>"><i
                                                class="fas fa-plus mr-1"></i> เพิ่มข้อมูล</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="get_users" class="table" style="font-size: 13px !important;">
                                    <thead class="thead-color">
                                        <tr class="text-center table-tr">
                                            <th width="10%"></th>
                                            <th width="20%">
                                                รูปห้อง
                                            </th>
                                            <th>ชื่อห้อง</th>
                                            <th>แถบสีสถานะห้องประชุม</th>
                                            <th>รายละเอียดห้องประชุม</th>
                                            <th width="15%">สถานะ</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-bordered">
                                        <?php
                                        if (isset($arrRooms)) {
                                            foreach ($arrRooms as $row) {
                                        ?>
                                                <tr>
                                                    <td class="text-center font-weight-bold">
                                                        <div class="btn-group">
                                                            <a href="<?= base_url('admin/rooms/edit/') . $row['room_id']; ?>"
                                                                class="btn btn-warning btn-sm" title="แก้ไข">
                                                                <i class=" fas fa-edit"></i></a>
                                                            <a href="" class="btn btn-danger btn-sm" title="ลบ"
                                                                data-toggle="modal" data-target="#del-list"
                                                                data-id="<?= $row['room_id']; ?>"
                                                                data-url="<?= base_url('admin/rooms/del/') . $row['room_id']; ?>"><i
                                                                    class="fas fa-trash"></i></a>
                                                        </div>
                                                    </td>
                                                    <td class="text-center font-weight-bold">
                                                        <img src="<?= base_url() . 'uploads/img/' . $row['room_image']; ?>"
                                                            alt="" width="100%">
                                                    </td>
                                                    <td>
                                                        <?= $row['room_name']; ?>
                                                    </td>
                                                    <td>
                                                        <?php if (isset($row['room_color'])): ?>
                                                            <div class="input-group">
                                                                <svg xmlns="http://www.w3.org/2000/svg" class="mr-2" viewBox="0 0 512 512" width="20px" fill="<?= $row['room_color_opacity']; ?>">
                                                                    <path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z" />
                                                                </svg>
                                                                รออนุมัติ
                                                            </div>
                                                            <div class="input-group mt-3">
                                                                <svg xmlns="http://www.w3.org/2000/svg" class="mr-2" viewBox="0 0 512 512" width="20px" fill="<?= $row['room_color']; ?>">
                                                                    <path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z" />
                                                                </svg>
                                                                อนุมัติแล้ว
                                                            </div>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td id="meetDetail">
                                                        <?= $row['room_detail']; ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php if ($row['room_active'] == 1) {
                                                            echo 'เปิดใช้งาน';
                                                        } else {
                                                            echo 'ปิดใช้งาน';
                                                        } ?>
                                                    </td>
                                                </tr>
                                        <?php }
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- Modal Del -->
                <div class="modal fade" id="del-list" data-backdrop="static" data-keyboard="false" tabindex="-1"
                    aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <form id="del_url" action="" method="post">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <label style="padding-top: 40px;"><b>คุณต้องการลบข้อมูลรายการนี้ใช่หรือไม่?</b>
                                    </label>
                                </div>
                                <div class="modal-footer" style="border-top: 0px;padding: 0px 10px 10px 0px;">
                                    <button type="submit" class="btn btn-primary btn-sm">ยืนยัน</button>
                                    <button type="button" class="btn btn-danger btn-sm"
                                        data-dismiss="modal">ยกเลิก</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>