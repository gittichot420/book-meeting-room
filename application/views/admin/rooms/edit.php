<style>
    .note-editable p {
        color: #000 !important;
    }

    .file {
        height: auto !important;
    }

    .input-sm {
        height: calc(1.8125rem + 8px) !important;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12 mt-4">
                    <!-- title -->
                    <h2 class="float-left txt-tilte-page"><i class="fa fa-users nav-icon"></i> แก้ไขข้อมูลห้องประชุม</h2>
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>" class="text-secondary txt-page">หน้าหลัก</a> ><a href="<?= base_url('admin/rooms') ?>" class="text-secondary txt-page">ห้องประชุม</a> > แก้ไขห้องประชุม</span>
                </div>
            </div>
            <!-- CONTENT -->
            <div class="card mt-4">
                <div class="card-body p-4">
                    <form action="<?= base_url('admin/rooms/edit/') . $_RoomsID['room_id']; ?>" method="post" enctype="multipart/form-data">
                        <!-- row -->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- row1 -->
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">ชื่อห้อง *</label>
                                            <input hidden type="text" id="get_img" name="get_img" value="<?= $_RoomsID['room_image']; ?>">
                                            <input type="text" id="room_name" name="room_name" class="form-control form-control-sm input-sm" value="<?= $_RoomsID['room_name']; ?>" required />
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">รูปห้อง *</label>
                                            <input type="file" id="room_image" name="room_image" class="form-control form-control-sm file" accept="image/*" />
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">สีห้อง *</label>
                                            <input type="color" id="room_color" name="room_color" class="form-control" value="<?php if (isset($_RoomsID['room_color'])) {
                                                                                                                                    echo $_RoomsID['room_color'];
                                                                                                                                } else {
                                                                                                                                    echo '';
                                                                                                                                } ?>" />
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">สถานะ</label>
                                            <?php
                                            $isChecked = '';
                                            if ($_RoomsID['room_active']) {
                                                $isChecked = 'checked';
                                            }
                                            ?>
                                            <input type="checkbox" id="active" name="active" class="form-control form-control-sm" value="1" style="height: calc(1.8125rem + -8px);" <?= $isChecked; ?> />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <label class="form-label form-regis" for="form2Example17">รายละเอียดห้อง *</label>
                                        <textarea class="form-control is-invalid" id="roomsDetail" name="room_detail" placeholder="Required example textarea">
                                        <?= $_RoomsID['room_detail']; ?>
                                        </textarea>
                                    </div>
                                    <?php if ($_RoomsID['room_image']) : ?>
                                        <div class="col-12">
                                            <label class="form-label form-regis" for="form2Example17">รูปห้อง</label> <br>
                                            <img src="/uploads/img/<?= $_RoomsID['room_image']; ?>" alt="" width="30%">
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-success mr-2">อัพเดท</button>
                                        <a href="<?= base_url('admin/rooms') ?>" class="btn btn-danger">ย้อนกลับ</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>