<style>
    .note-editable p {
        color: #000 !important;
    }

    .file {
        height: auto !important;
    }

    .input-sm {
        height: calc(1.8125rem + 8px) !important;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12 mt-4">
                    <!-- title -->
                    <h2 class="float-left txt-tilte-page"><i class="fa fa-users nav-icon"></i> เพิ่มข้อมูลห้องประชุม</h2>
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>" class="text-secondary txt-page">หน้าหลัก</a> ><a href="<?= base_url('admin/rooms') ?>" class="text-secondary txt-page">ห้องประชุม</a> > เพิ่มห้องประชุม</span>
                </div>
            </div>
            <!-- CONTENT -->
            <div class="card mt-4">
                <div class="card-body p-4">
                    <form action="<?= base_url('admin/rooms/add'); ?>" method="post" enctype="multipart/form-data">
                        <!-- row -->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- row1 -->
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">ชื่อห้อง *</label>
                                            <input type="text" id="room_name" name="room_name" class="form-control form-control-sm input-sm" required />
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">รูปห้อง *</label>
                                            <input type="file" id="room_image" name="room_image" class="form-control form-control-sm file" accept="image/*" />
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">สีห้อง *</label>
                                            <input type="color" id="room_color" name="room_color" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <label class="form-label form-regis" for="form2Example17">รายละเอียดห้อง *</label>
                                        <textarea class="form-control is-invalid" id="roomsDetail" name="room_detail"></textarea>


                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-success mr-2">บันทึก</button>
                                        <a href="<?= base_url('admin/rooms') ?>" class="btn btn-danger">ย้อนกลับ</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>