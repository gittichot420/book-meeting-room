<!DOCTYPE html>
<html lang="th">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= base_url('lib/jquery.fancybox.css'); ?>" type="text/css" media="screen" />
    <link href='<?= base_url('fullcalendar/fullcalendar.css'); ?>' rel='stylesheet' />
    <link href='<?= base_url('fullcalendar/fullcalendar.print.css'); ?>' rel='stylesheet' media='print' />
    <link href="<?= base_url('lib/bootstrap/dist/css/bootstrap.css'); ?>" rel="stylesheet">
    <!-- <script src="lib/jquery/dist/jquery.min.js"></script> -->
    <script src="<?= base_url('lib/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- Custom Theme JavaScript -->
    <script src='<?= base_url('lib/moment.min.js'); ?>'></script>
    <script src='<?= base_url('fullcalendar/fullcalendar.min.js'); ?>'></script>
    <script src='<?= base_url('lib/lang/th.js'); ?>'></script>
    <script src="<?= base_url('lib/jquery.fancybox.pack.js'); ?>"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script> -->
</head>
<?php
if ($events['meet_status'] == 1) {
    $status =
        "<button class='btn btn-success btn-sm'>" .
        "<i class='fa fa-check pr-2'></i> อนุมัติ </button>";
}
// elseif ($events['ev_status'] == 'reject') {
//     $status =
//         "<button class='btn btn-danger btn-sm'>" .
//         "<i class='fas fa-ban'></i> ไม่อนุมัติ</button>";
// } 
elseif ($events['meet_status'] == 0) {
    $status =
        "<button class='btn btn-warning btn-sm'>" .
        "<i class='fas fa-spinner'></i>  รออนุมัติ</button>";
}

if ($events['meet_status'] == 1) {
    $status2 =
        "<button class='btn btn-success btn-sm'>" .
        "<i class='fa fa-check pr-2'></i> ตรวจสอบแล้ว </button>";
} elseif ($events['meet_status'] == 'reject') {
    $status2 =
        "<button class='btn btn-danger btn-sm'>" .
        "<i class='fa fa-remove pr-2'></i> อนุมัติ / ยกเลิก</button>";
} elseif ($events['meet_status'] == 0) {
    $status2 =
        "<div class='btn btn-warning btn-sm'>" .
        "<i class='fa fa-refresh pr-2'></i>  อนุมัติ / รอใช้</button>";
} else {
    $status2 =
        "<button class='btn btn-warning btn-sm'>" .
        "<i class='fa fa-refresh pr-2'></i>  อนุมัติ / รอใช้</button>";
}
?>
<style>
    .custom-link a {
        color: #000000;
        text-decoration: none;
    }

    .custom-link a:hover {
        color: blue;
        text-decoration: underline;
    }
</style>
<div id="wrapper">

    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-8" style="margin-top: 50px;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <b>เรื่อง:</b> <?= $events['meet_title'] ?>
                    </div>
                    <div class="panel-body">
                        <div class="btn-sm"><b> วัน-เวลา เริ่มจ่ายหนังสือ</b></div>
                        <div class="alert alert-success" style='color: #000000;'>
                            <?= date("d-m-Y", strtotime($events['meet_date_start'])) . ' เวลา:' . $events['meet_time_start'] ?>
                        </div>
                        <div class="btn-sm"><b>วัน-เวลา กำหนดส่งหนังสือ</b></div>
                        <div class="alert alert-warning" style='color: #000000;'>
                            <?= date("d-m-Y", strtotime($events['meet_date_end'])) . ' เวลา:' . $events['meet_time_end'] ?>
                        </div>
                        <?php if ($events['meet_status'] == 'accept') {
                            echo $status2;
                        } else {
                            echo $status;
                        } ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
            </div>
        </div>
    </div>
</div>