<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Meeting Rooms</title>
    <!-- css -->
    <link rel="stylesheet" href="<?= base_url('assets/css/app.css'); ?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet" />
    <!-- <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('/assets/plugins/fontawesome-free/css/all.min.css'); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css'); ?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css'); ?>">
    <!-- JQVMap -->
    <!-- <link rel="stylesheet" href="<?= base_url('assets/plugins/jqvmap/jqvmap.min.css'); ?>"> -->
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/adminlte.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/style.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/badges.css'); ?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css'); ?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/daterangepicker/daterangepicker.css'); ?>">
    <!-- summernote -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/summernote/summernote-bs4.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css'); ?>">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    <!-- icon -->
    <!-- <link rel="icon" type="image/png" href="<?= base_url('./assets/img/logo.gif') ?>" /> -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <link rel="stylesheet" href="<?= base_url('lib/jquery.fancybox.css'); ?>" type="text/css" media="screen" />
    <link href='<?= base_url('fullcalendar/fullcalendar.css'); ?>' rel='stylesheet' />
    <link href='<?= base_url('fullcalendar/fullcalendar.print.css'); ?>' rel='stylesheet' media='print' />
    <script src="<?= base_url('lib/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- Custom Theme JavaScript -->
    <script src='<?= base_url('lib/moment.min.js'); ?>'></script>
    <script src='<?= base_url('fullcalendar/fullcalendar.min.js'); ?>'></script>
    <script src='<?= base_url('lib/lang/th.js'); ?>'></script>
    <script src="<?= base_url('lib/jquery.fancybox.pack.js'); ?>"></script>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <!-- css -->
    <style>
        sup {
            color: red;
        }

        .note-editable p {
            color: #000 !important;
        }

        /* .req {
            background-color: #ffcec5;
            border-color: #ffcec5;
        } */

        input[type="date"]::-webkit-datetime-edit,
        input[type="date"]::-webkit-inner-spin-button,
        input[type="date"]::-webkit-clear-button {
            color: #fff;
            position: relative;
        }

        input[type="date"]::-webkit-datetime-edit-year-field {
            position: absolute !important;
            padding: 2px;
            color: #000;
            left: 56px;
        }

        input[type="date"]::-webkit-datetime-edit-month-field {
            position: absolute !important;
            padding: 2px;
            color: #000;
            left: 26px;
        }


        input[type="date"]::-webkit-datetime-edit-day-field {
            position: absolute !important;
            color: #000;
            padding: 2px;
            left: 4px;

        }

        .border-radius-left {
            border-bottom-left-radius: 0 !important;
            border-top-left-radius: 0 !important;
        }

        .border-radius-right {
            border-bottom-right-radius: 0 !important;
            border-top-right-radius: 0 !important;
        }
    </style>
    <!-- active path -->
    <script>
        $(document).ready(function() {
            var path = location.pathname.split('/').pop();
            if (path == '') {
                path = 'dashboard';
            }
            var target = $('nav a[id="' + path + '"]');
            target.addClass('active');
        });
    </script>

</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <nav id="hmenu" class="main-header navbar navbar-expand navbar-white navbar-light">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link pushmenu" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <div class="navbar-search-block">
                        <form class="form-inline">
                            <div class="input-group input-group-sm">
                                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-navbar" type="submit">
                                        <i class="fas fa-search"></i>
                                    </button>
                                    <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </li>
                <li class=" nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-user pr-1"></i>
                        <?php if (!empty($this->session->userdata('users'))) {
                            echo $this->session->userdata('users')['u_fname'] . ' ' . $this->session->userdata('users')['u_lname'];
                        } ?> <i class="fas fa-angle-down pl-1"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="<?= base_url('auth/logout'); ?> " class="dropdown-item dropdown-footer "><i class="fas fa-sign-in-alt pr-1"></i> ออกจากระบบ</a>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar elevation-4">
            <!-- Brand Logo -->
            <li class="brand-link mt-3 pl-2 pl-2">
                <img src="<?= base_url('assets/img/logo.png'); ?>" alt="Logo" class="" style="opacity: 0.8" width="100%">
            </li>
            <!-- Sidebar -->
            <div class="sidebar">
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-header"></li>
                        <!-- Dashboard -->
                        <li class="nav-item">
                            <a id="dashboard" href="<?= base_url('admin/dashboard') ?>" class="nav-link <?php if (isset($menu_page)) {
                                                                                                            if ($menu_page == 'dashboard') {
                                                                                                                echo 'active';
                                                                                                            }
                                                                                                        } ?>">
                                <i class="fas fa-home nav-icon"></i>
                                <p>
                                    หน้าหลัก
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a id="meeting" href="<?= base_url('admin/meeting') ?>" class="nav-link <?php if (isset($menu_page)) {
                                                                                                        if ($menu_page == 'meeting') {
                                                                                                            echo 'active';
                                                                                                        }
                                                                                                    } ?>">
                                <i class="fas fa-book nav-icon"></i>
                                <p>
                                    รายการจองห้องประชุม
                                </p>
                            </a>
                        </li>

                        <li class="nav-header">จัดการ</li>
                        <li class="nav-item">
                            <a id="rooms" href="<?= base_url('admin/rooms') ?>" class="nav-link  <?php if (isset($menu_page)) {
                                                                                                        if ($menu_page == 'rooms') {
                                                                                                            echo 'active';
                                                                                                        }
                                                                                                    } ?>">
                                <i class="fas fa-table nav-icon"></i>
                                <p>ห้องประชุม</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a id="users" href="<?= base_url('admin/users') ?>" class="nav-link  <?php if (isset($menu_page)) {
                                                                                                        if ($menu_page == 'users') {
                                                                                                            echo 'active';
                                                                                                        }
                                                                                                    } ?>">
                                <i class="fa fa-users nav-icon"></i>
                                <p>สมาชิก</p>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
    </div>