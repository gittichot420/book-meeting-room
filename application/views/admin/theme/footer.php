<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>

<script>
    $('.timepicker').timepicker({
        timeFormat: 'HH:mm',
        interval: 15,
        minTime: '6',
        maxTime: '18',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
</script>
<!-- jQuery -->
<script src="<?= base_url('lib/jquery/dist/jquery.min.js'); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?= base_url('assets/plugins/jquery-ui/jquery-ui.min.js'); ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<!-- ChartJS -->
<script src="<?= base_url('assets/plugins/chart.js/Chart.min.js'); ?> "></script>
<!-- jQuery Knob Chart -->
<script src="<?= base_url('assets/plugins/jquery-knob/jquery.knob.min.js'); ?> "></script>
<!-- daterangepicker -->
<script src="<?= base_url('assets/plugins/moment/moment.min.js'); ?> "></script>
<script src="<?= base_url('assets/plugins/daterangepicker/daterangepicker.js'); ?> "></script>

<!-- Tempusdominus Bootstrap 4 -->
<script src="<?= base_url('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js'); ?> ">
</script>
<!-- Summernote -->
<script src="<?= base_url('assets/plugins/summernote/summernote-bs4.min.js'); ?> "></script>
<!-- overlayScrollbars -->
<script src="<?= base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js'); ?> "></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('assets/dist/js/demo.js'); ?>"></script>

<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/jszip/jszip.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/pdfmake/pdfmake.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/pdfmake/vfs_fonts.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.html5.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.print.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.colVis.min.js'); ?>"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>

<script src="<?= base_url('assets/dist/js/adminlte.min.js'); ?>"></script>

<!-- Modal -->
<script src="<?= base_url('assets/js/modal.js'); ?>"></script>

<!-- Sweetalert -->
<?php include 'sweetalert.php'; ?>

<script>
    function meetingApprove(meet_id) {
        $.ajax({
            url: "meeting/ajax_update_meetimg",
            type: 'post',
            dataType: "json",
            data: {
                'meet_id': meet_id,
            },
            success: (res) => {
                Swal.fire({
                    title: "สำเร็จ",
                    text: "อนุมัติสำเร็จ",
                    icon: "success",
                    timer: 3000,
                }).then((resule) => {
                    location.reload();
                });
            },
        });

    }
    var check = function() {
        var pass = $('#password').val();
        var confirm_password = $('#confirm_password').val();

        if (pass && confirm_password != null) {
            if (pass == confirm_password) {
                $('#message').css('color', 'green');
                $('#message').html(' รหัสผ่านตรง');
                $('#password').css('border-color', 'green');
                $('#confirm_password').css('border-color', 'green');
            } else {
                $('#message').css('color', 'red');
                $('#message').html(' รหัสผ่านไม่ตรงกัน');
                $('#password').css('border-color', 'red');
                $('#confirm_password').css('border-color', 'red');
            }
        } else {
            $('#message').html('');
        }
    }


    // $(document).ready(function() {
    //     $('#summernote').summernote({
    //         placeholder: 'รายละเอียดเพิ่มเติม',
    //     });

    $('#roomsDetail').summernote({
        placeholder: 'ชื่อผู้ดูแลห้อง <br><br><br><br>เบอร์โทรผู้ดูแลห้อง<br><br><br><br>จำนวนจุคน<br><br><br><br>',
        height: '400px',
    });

    //     $("#example1").DataTable({
    //         "responsive": true,
    //         "lengthChange": true,
    //         "autoWidth": false,
    //         "pageLength": 25,
    //         "lengthChange": false,
    //         language: {
    //             url: '//cdn.datatables.net/plug-ins/1.13.7/i18n/th.json',
    //         },
    //         // "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    //     }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    //     $('#table-slip').DataTable({
    //         "responsive": true,
    //         "lengthChange": true,
    //         "autoWidth": false,
    //         "pageLength": 25,
    //         "lengthChange": false,
    //         order: [
    //             [0, 'asc'],
    //             [1, 'asc'],
    //         ],
    //         language: {
    //             url: '//cdn.datatables.net/plug-ins/1.13.7/i18n/th.json',
    //         }
    //     });

    //     $('#assign_tb').DataTable({
    //         "responsive": true,
    //         "lengthChange": true,
    //         "autoWidth": false,
    //         "pageLength": 25,
    //         "lengthChange": false,
    //         order: [
    //             [0, 'asc'],
    //         ],
    //         language: {
    //             url: '//cdn.datatables.net/plug-ins/1.13.7/i18n/th.json',
    //         }
    //     });

    //     $('#group_order').DataTable({
    //         "responsive": true,
    //         "lengthChange": true,
    //         "autoWidth": false,
    //         "pageLength": 25,
    //         "lengthChange": false,
    //         order: [
    //             [2, 'desc'],
    //             [1, 'asc'],
    //         ],
    //         language: {
    //             url: '//cdn.datatables.net/plug-ins/1.13.7/i18n/th.json',
    //         }
    //     });

    //     $('#cat-table').DataTable({
    //         "responsive": true,
    //         "lengthChange": true,
    //         "autoWidth": false,
    //         "pageLength": 25,
    //         "lengthChange": false,
    //         order: [
    //             [1, 'asc'],
    //             [2, 'asc'],
    //         ],
    //         language: {
    //             url: '//cdn.datatables.net/plug-ins/1.13.7/i18n/th.json',
    //         }
    //     });

    $('#get_users').DataTable({
        "responsive": true,
        "lengthChange": true,
        "autoWidth": false,
        "pageLength": 25,
        "lengthChange": false,
        order: [
            [0, 'asc'],
            [1, 'asc'],
        ],
        language: {
            url: '//cdn.datatables.net/plug-ins/1.13.7/i18n/th.json',
        }
    });

    $('#meetingRooms').DataTable({
        "responsive": true,
        "lengthChange": true,
        "autoWidth": false,
        "pageLength": 25,
        "lengthChange": false,
        order: [
            [1, 'desc'],
        ],
        language: {
            url: '//cdn.datatables.net/plug-ins/1.13.7/i18n/th.json',
        }
    });

    //     $('#data-table').DataTable({
    //         "responsive": true,
    //         "lengthChange": true,
    //         "autoWidth": false,
    //         "pageLength": 25,
    //         "lengthChange": false,
    //         order: [
    //             [1, 'asc'],
    //         ],
    //         language: {
    //             url: '//cdn.datatables.net/plug-ins/1.13.7/i18n/th.json',
    //         }
    //     });

    //     $('#list_product').DataTable({
    //         "responsive": true,
    //         "lengthChange": true,
    //         "autoWidth": false,
    //         "pageLength": 25,
    //         "lengthChange": false,
    //         order: [
    //             [4, 'desc'],
    //             [3, 'asc'],
    //         ],
    //         language: {
    //             url: '//cdn.datatables.net/plug-ins/1.13.7/i18n/th.json',
    //         }
    //     });

    //     $('#pd_img').on('change', function() {
    //         var fileName_product = $('#pd_img').val();
    //         var product = fileName_product.replace('C:\\fakepath\\', " ");
    //         $('#pd_img_text').text(product);
    //     });
    // });
</script>

</body>

</html>