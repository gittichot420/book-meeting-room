<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12 mt-4">
                    <!-- title -->
                    <h2 class="float-left txt-tilte-page"><i class="fa fa-users nav-icon"></i> แก้ไขข้อมูลสมาชิก</h2>
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>" class="text-secondary txt-page">หน้าหลัก</a> ><a href="<?= base_url('admin/users') ?>" class="text-secondary txt-page">สมาชิก</a> > แก้ไขสมาชิก</span>
                </div>
            </div>
            <!-- CONTENT -->
            <div class="card mt-4">
                <div class="card-body">
                    <form action="<?= base_url('admin/users/edit/') . $_userID['u_id']; ?>" method="get">
                        <!-- row -->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- row1 -->
                                <div class="row">
                                    <div class="col-md-2">
                                        <!-- คำนำหน้า -->
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">คำนำหน้า
                                                *</label>
                                            <select class="form-control form-control-sm" id="prefix" name="prefix" required>
                                                <option value="" disabled>-เลือก-</option>
                                                <?php
                                                $_arrPrefix = ['นาย', 'นาง', 'นางสาว', 'เด็กชาย', 'เด็กหญิง'];
                                                $selected = '';
                                                foreach ($_arrPrefix as $prefix) :
                                                    if ($_userID['u_prefix'] == $prefix) {
                                                        $selected = 'selected';
                                                    } else {
                                                        $selected = '';
                                                    }
                                                ?>
                                                    <option value="<?= $prefix; ?>" <?= $selected; ?>><?= $prefix; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <!-- ชื่อ -->
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">ชื่อ *</label>
                                            <input id="fname" name="fname" type="text" class="form-control form-control-sm" value="<?= $_userID['u_fname']; ?>" required />
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <!-- นามสกุล -->
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">นามสกุล
                                                *</label>
                                            <input id="lname" name="lname" type="text" class="form-control form-control-sm" value="<?= $_userID['u_lname']; ?>" required />
                                        </div>
                                    </div>
                                </div>
                                <!-- row2 -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">ตำแหน่ง </label>
                                            <input id="position" name="position" type="text" class="form-control form-control-sm" value="<?= $_userID['u_position']; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-outline text-left mb-3">
                                            <label class="form-label form-regis" for="form2Example17">Enable </label>
                                            <div class="form-outline pl-5">
                                                <input id="active" name="active" type="checkbox" class="form-check-input" value="1" <?php if ($_userID['u_active'] == '1') {
                                                                                                                                        echo 'checked';
                                                                                                                                    } ?> />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <!-- row3 -->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">ชื่อผู้ใช้งาน </label>
                                            <input id="username" name="username" type="text" class="form-control form-control-sm" value="<?= $_userID['u_user']; ?>" readonly />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">รหัสผ่าน
                                            </label>
                                            <input id="password" name="password" type="password" class="form-control form-control-sm" oninput='check();' />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-outline mb-3">
                                            <label class="form-label form-regis" for="form2Example17">ยืนยันรหัสผ่าน
                                            </label>
                                            <input id="confirm_password" name="confirm_password" type="password" class="form-control form-control-sm" oninput='check();' />
                                            <label id='message' class="text-center"></label>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-success ">อัพเดท</button>
                                        <a href="<?= base_url('admin/users') ?>" class="btn btn-danger">ย้อนกลับ</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>