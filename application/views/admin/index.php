<style>
    .bg-primary {
        background-color: #3f9bff !important;

    }

    .bg-warning:hover {
        background-color: #e1a900 !important;
    }

    .bg-success:hover {
        background-color: #00841e !important;
    }

    #meetDetail p {
        color: #34495e !important;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper ">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row mb-5">
                <!-- Left col -->
                <div class="col-md-3 mt-4">
                    <!-- title -->
                </div>
                <div class="col-md-9 mt-4">
                    <span class="text-secondary text-sm float-right">หน้าหลัก</span>
                </div>
            </div>

            <!-- Small boxes (Stat box) -->
            <div class="mb-5">
                <div class="row">
                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-4 col-md-6">
                        <div class="info-box py-3 px-3 bg-primary">
                            <span class="info-box-icon  px-5">
                                <i class="fas fa-clipboard-list fa-2x"></i>
                                <!-- <i class="fas fa-user-edit"></i> -->
                            </span>
                            <div class="info-box-content pl-3">
                                <span class="info-box-text text-bold" style="font-size:20px">จองห้องประชุม</span>
                                <span class="info-box-number p-0 m-0" style="font-size:20px">
                                    <?php if (isset($sum_meet_date)) {
                                        echo $sum_meet_date;
                                    } else {
                                        echo '0';
                                    } ?>
                                    <small>การจองวันนี้</small>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="info-box py-3 px-3 bg-warning">
                            <span class="info-box-icon px-5">
                                <i class="far fa-calendar-alt fa-2x text-white"></i>
                            </span>
                            <div class="info-box-content pl-3">
                                <span class="info-box-text text-bold text-white"
                                    style="font-size:20px">ห้องประชุมทั้งหมด</span>
                                <span class="info-box-number p-0 m-0 text-white" style="font-size:20px">
                                    <?php if (isset($sum_room)) {
                                        echo $sum_room;
                                    } else {
                                        echo '0';
                                    } ?>
                                    <small>ห้อง</small>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="info-box py-3 px-3  bg-success">
                            <span class="info-box-icon px-5">
                                <i class="fas fa-check fa-2x"></i>
                            </span>
                            <div class="info-box-content pl-3">
                                <span class="info-box-text text-bold" style="font-size:20px">สถิติรายงานการจอง</span>
                                <span class="info-box-number p-0 m-0" style="font-size:20px">
                                    <?php if (isset($sum_meet_month)) {
                                        echo $sum_meet_month;
                                    } else {
                                        echo '0';
                                    } ?>
                                    <small>การจองเดือนนี้</small>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <!-- Main row -->
            <style>
                svg {
                    border: 1px solid #a0a0a0;
                    border-radius: 100%;
                }
            </style>
            <div class="row">
                <!-- Left col -->
                <!-- <section class="col-lg-12 connectedSortable"> -->
                <section class="col-lg-12">
                    <!-- Custom tabs (Charts with tabs)-->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="far fa-calendar-alt mr-1"></i>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <!-- group_assign -->
                            <div id='calendar'></div>
                            <div class="row mt-5">
                                <?php foreach ($arrRooms as $rooms):  ?>
                                    <div class="col-6 col-md-3 col-lg-2 pb-3">
                                        <div class="card p-1" style="background-color: <?= $rooms['room_color_opacity']; ?>;">
                                            <small class="d-block text-lg-center">ห้องประชุม: <b style="font-weight: 500;"><?= $rooms['room_name']; ?> </b></small>
                                            <div class=" input-group">
                                                <small>
                                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="10px" fill="<?= $rooms['room_color_opacity']; ?>">
                                                        <path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z" />
                                                    </svg>
                                                    รออนุมัติ
                                                </small>
                                                <small>
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="ml-lg-2" viewBox="0 0 512 512" width="10px" fill="<?= $rooms['room_color']; ?>">
                                                        <path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z" />
                                                    </svg>
                                                    อนุมัติแล้ว
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach;  ?>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </section>
                <!-- /.Left col -->
            </div>
            <!-- /.row (main row) -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>


<div id="calendarModal" class="modal fade" style="z-index: 1400;">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f1f1f1;">
                <h5 id="modalTitle" class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>
                            <td width="40%"><b>วัน-เวลาเริ่ม: </b><br>
                                <b>วัน-เวลาสิ้นสุด:</b>
                            </td>
                            <td id="meetingDate"></td>
                        </tr>
                        <tr>
                            <td><b>ห้องประชุม:</b> </td>
                            <td id="roomName"></td>
                        </tr>
                        <tr>
                            <td><b>ข้อมูลผู้จองห้องประชุม:</b> </td>
                            <td id="meetName"></td>
                        </tr>
                        <tr>
                            <td><b>จำนวนผู้เข้าร่วมประชุม:</b> </td>
                            <td id="meetUnit"></td>
                        </tr>
                        <tr>
                            <td><b>รายละเอียด:</b> </td>
                            <td id="meetDetail"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div><!-- /.modal -->

<!-- 
<script>
    $(document).ready(function() {
        $('#calendar').fullCalendar({
            header: {
                left: '',
                center: 'title',
                right: 'today prev,next'
            },
            events: {
                url: '/ajax/calendar',
            },
            businessHours: true,
            eventLimit: true,
            timezone: 'Asia/Bangkok',
            height: 'auto',
            lang: 'th'
        });

    });
</script> -->

<script>
    var events = <?= json_encode($data) ?>;
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        titleFormat: {
            month: 'MMMM YYYY'
        },
        viewRender: function(view, element) {
            setTimeout(function() {
                var strDate = $.trim($(".fc-center").find("h2").text());
                var arrDate = strDate.split(" ");
                var lengthArr = arrDate.length;
                var newstrDate = "";
                for (var i = 0; i < lengthArr; i++) {
                    if (lengthArr - 1 == i || parseInt(arrDate[i]) > 1000) {
                        var yearBuddha = parseInt(arrDate[i]) + 543;
                        newstrDate += yearBuddha;
                    } else {
                        newstrDate += arrDate[i] + " ";
                    }
                }
                $(".fc-center").find("h2").text(newstrDate);
            }, 5);
        },
        height: 'parent',
        events: events,
        eventLimit: true,
        displayEventEnd: true,
        defaultDate: new Date(),
        timezone: 'Asia/Bangkok',
        loading: function(bool) {
            $('#loading').toggle(bool);
        },
        height: 650,
        eventClick: function(events) {
            var meetStatus = '';
            if (events.status == 1) {
                meetStatus =
                    '  <sup><span class="badge badge-pill" style="color: white;font-size:12px;background-color: ' + events.color + ';">อนุมัติแล้ว</span></sup>';
            }
            if (events.status == 0) {
                meetStatus =
                    '  <sup><span class="badge badge-pill" style="color: white;font-size:12px;background-color: ' + events.color + ';">รออนุมัติ</span></sup>';
            }

            $('#modalTitle').html(events.title + meetStatus);
            var fdate = events.date_start + ' ' + events.ftime_start + ' น. <br>' + events.date_end + ' ' +
                events.ftime_end + ' น.';
            $('#meetingDate').html(fdate);
            $('#meetName').html(events.name);
            $('#meetUnit').html(events.unit + ' คน');
            $('#roomName').html(events.room);
            $('#meetDetail').html(events.detail);
            $('#calendarModal').modal();
        },
        editable: true,
        dayMaxEvents: true,
    });
</script>