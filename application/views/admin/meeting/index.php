<!-- Content Wrapper. Contains page content -->
<style>
    /* #get_users td:nth-child(7),
th:nth-child(7) {
    display: none;
} */
</style>

<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">

            </div>
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12 mt-4">
                    <!-- title -->
                    <h2 class="float-left txt-tilte-page"><i class="fa fa-book  nav-icon"></i> รายการจองห้องประชุม
                    </h2>
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>" class="text-secondary txt-page">หน้าหลัก</a> > รายการจอง</span>
                </div>
            </div>
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <section class="col-md-12">
                    <!-- DIRECT CHAT -->
                    <div class="card mt-4">
                        <!-- <div class="card-header">
                            <div class="card-tools">
                                <ul class="nav nav-pills ml-auto">
                                    <li class="nav-item">
                                        <a class="btn btn-success btn-sm" href="<?= base_url('admin/users/add'); ?>"><i class="fas fa-plus mr-1"></i> เพิ่มข้อมูล</a>
                                    </li>
                                </ul>
                            </div>
                        </div> -->
                        <!--
                        รายการแก้ไขได้ 
                        -วันที่, สถานะ
                        -->
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="meetingRooms" class="table" style="font-size: 13px !important;">
                                    <thead class="thead-color">
                                        <tr class="text-center table-tr">
                                            <th></th>
                                            <th>วัน-เวลาเริ่ม</th>
                                            <th>วัน-เวลาสิ้นสุด</th>
                                            <th>ห้องประชุมที</th>
                                            <th>หัวข้อประชุม (เรื่อง)</th>
                                            <th>แถบสีสถานะห้องประชุม</th>
                                            <th>ผู้เข้าร่วม</th>
                                            <th>ผู้จอง</th>
                                            <th>สถานะ</th>
                                        </tr>
                                    </thead>
                                    <tbody class="table-bordered">
                                        <?php
                                        $num = 0;
                                        if (isset($arrMeeting)) {
                                            foreach ($arrMeeting as $row) {
                                                $num++;
                                        ?>
                                                <tr>
                                                    <td class="text-center font-weight-bold" width="10%">
                                                        <div class="btn-group">
                                                            <?php if ($row['meet_status'] == 0) { ?>
                                                                <button onclick="meetingApprove('<?= $row['meet_id']; ?>')" class="btn btn-success btn-sm" title="แก้ไข">
                                                                    <i class=" fas fa-check"></i></button>
                                                            <?php } ?>
                                                            <a href="<?= base_url('admin/meeting/edit/') . $row['meet_id']; ?>" class="btn btn-warning btn-sm" title="แก้ไข">
                                                                <i class=" fas fa-edit"></i></a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <?= $row['meet_date_start']; ?> <br>
                                                        <?= $row['meet_time_start']; ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['meet_date_end']; ?> <br>
                                                        <?= $row['meet_time_end']; ?>
                                                    </td>
                                                    <td class="text-bold" style="font-size: 20px;">
                                                        <?= $row['room_name']; ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['meet_title']; ?>
                                                    </td>
                                                    <td>
                                                        <?php if (isset($row['room_color'])): ?>
                                                            <div class="input-group">
                                                                <svg xmlns="http://www.w3.org/2000/svg" class="mr-2" viewBox="0 0 512 512" width="20px" fill="<?= $row['room_color_opacity']; ?>">
                                                                    <path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z" />
                                                                </svg>
                                                                รออนุมัติ
                                                            </div>
                                                            <div class="input-group mt-3">
                                                                <svg xmlns="http://www.w3.org/2000/svg" class="mr-2" viewBox="0 0 512 512" width="20px" fill="<?= $row['room_color']; ?>">
                                                                    <path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z" />
                                                                </svg>
                                                                อนุมัติแล้ว
                                                            </div>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?= $row['meet_unit']; ?>
                                                    </td>
                                                    <td>
                                                        ชื่อ: <?= $row['meet_name']; ?> <br>
                                                        ตำแหน่ง: <?= $row['meet_position']; ?> <br>
                                                        เบอร์: <?= $row['meet_tell']; ?>
                                                    </td>
                                                    <td class="text-center" style="font-size: 16px;">
                                                        <?php if ($row['meet_status'] == 0) {
                                                            echo '<span class="badge badge-warning">รออนุมัติ</span>';
                                                        } else if ($row['meet_status'] == 1) {
                                                            echo '<span class="badge badge-success">อนุมัติ</span>';
                                                        } else {
                                                            echo '<span class="badge badge-danger">ยกเลิก</span>';
                                                        } ?>
                                                    </td>
                                                </tr>
                                        <?php }
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- Modal Del -->
                <div class="modal fade" id="del-list" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <form id="del_url" action="" method="post">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <label style="padding-top: 40px;"><b>คุณต้องการลบข้อมูลรายการนี้ใช่หรือไม่?</b>
                                    </label>
                                </div>
                                <div class="modal-footer" style="border-top: 0px;padding: 0px 10px 10px 0px;">
                                    <button type="submit" class="btn btn-primary btn-sm">ยืนยัน</button>
                                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">ยกเลิก</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>