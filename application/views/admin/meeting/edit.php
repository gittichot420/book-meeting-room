<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12 mt-4">
                    <!-- title -->
                    <h2 class="float-left txt-tilte-page"><i class="fa fa-users nav-icon"></i> แก้ไขข้อมูลสมาชิก</h2>
                    <span class="text-secondary text-sm float-right"><a href="<?= base_url('admin/dashboard') ?>" class="text-secondary txt-page">หน้าหลัก</a> ><a href="<?= base_url('admin/users') ?>" class="text-secondary txt-page">สมาชิก</a> > แก้ไขสมาชิก</span>
                </div>
            </div>
            <!-- CONTENT -->
            <div class="card mt-4">
                <div class="card-body">
                    <form action="<?= base_url('admin/meeting/edit/') . $id; ?>" method="get">
                        <!-- row -->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- row1 -->
                                <div class="row">
                                    <!-- Meeting Room -->
                                    <div class="col-12">
                                        <div class="form-outline mb-3 ">
                                            <label for="">ห้องประชุม <sup>*</sup></label>
                                            <select class="custom-select" id="room_id" name="room_id" required>
                                                <option value="" disabled> -โปรดเลือก- </option>
                                                <?php
                                                $selected = '';
                                                if (isset($_getRooms)) :
                                                    foreach ($_getRooms as $rooms) :
                                                        if ($rooms['room_id'] == $_meetingID['room_id']) {
                                                            $selected = 'selected';
                                                        } else {
                                                            $selected = '';
                                                        }
                                                ?>
                                                        <option value="<?= $rooms['room_id'] ?>" <?= $selected ?>><?= $rooms['room_name'] ?></option>
                                                <?php
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-outline mb-3 ">
                                            <label for="">หัวข้อประชุม (เรื่อง) <sup>*</sup></label>
                                            <input type="text" id="meet_title" name="meet_title" class="form-control" value="<?= $_meetingID['meet_title'] ?>" required />
                                        </div>
                                        <div class="form-outline mb-3 ">
                                            <label for="">รายละเอียด</label>
                                            <textarea type="text" id="roomsDetail" name="meet_detail" class="form-control detail" value=""><?= $_meetingID['meet_detail'] ?></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="form-outline mb-3 col-12 col-lg-3">
                                                <label for="">จำนวนผู้เข้าร่วมประชุม <sup>*</sup></label>
                                                <input type="text" id="meet_unit" name="meet_unit" class="form-control" value="<?= $_meetingID['meet_unit'] ?>" required />
                                            </div>
                                            <div class="form-outline mb-3 col-12 col-lg-3">
                                                <label for="">ชื่อผู้จองห้องประชุม <sup>*</sup></label>
                                                <input type="text" id="meet_name" name="meet_name" class="form-control" value="<?= $_meetingID['meet_name'] ?>" required />
                                            </div>
                                            <div class="form-outline mb-3 col-12 col-lg-3">
                                                <label for="">สังกัดผู้จองห้องประชุม <sup>*</sup></label>
                                                <input type="text" id="meet_position" name="meet_position" class="form-control" value="<?= $_meetingID['meet_position'] ?>" required />
                                            </div>
                                            <div class="form-outline mb-3 col-12 col-lg-3">
                                                <label for="">เบอร์โทรติดต่อ <sup>*</sup></label>
                                                <input type="number" id="meet_tell" name="meet_tell" class="form-control" value="<?= $_meetingID['meet_tell'] ?>" required />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-outline mb-3 col-12 col-lg-3">
                                                <label for="">วันเริ่มประชุม <sup>*</sup></label>
                                                <input type="date" id="meet_date_start" name="meet_date_start" class="form-control" value="<?= $_meetingID['meet_date_start'] ?>" required>
                                            </div>
                                            <div class="form-outline mb-3 col-12 col-lg-3">
                                                <label for="">เวลาเริ่มประชุม <sup>*</sup></label>
                                                <div class="input-group-prepend">
                                                    <input type="text" id="meet_time_start" name="meet_time_start" class="form-control timepicker border-radius-right" value="<?= $_meetingID['meet_time_start'] ?>" required>
                                                    <span class="input-group-text  border-radius-left ">น.</span>
                                                </div>
                                            </div>
                                            <div class="form-outline mb-3 col-12 col-lg-3">
                                                <label for="">วันสิ้นสุดประชุม <sup>*</sup></label>
                                                <input type="date" id="meet_date_end" name="meet_date_end" class="form-control" value="<?= $_meetingID['meet_date_end'] ?>" required>
                                            </div>
                                            <div class="form-outline mb-3 col-12 col-lg-3">
                                                <label for="">เวลาสิ้นสุดประชุม <sup>*</sup></label>
                                                <div class="input-group-prepend">
                                                    <input type="text" id="meet_time_end" name="meet_time_end" class="form-control timepicker border-radius-right" value="<?= $_meetingID['meet_time_end'] ?>" required>
                                                    <span class="input-group-text  border-radius-left ">น.</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-outline mb-3 col-12 col-lg-3">
                                                <label for="">สถานะ <sup>*</sup></label>
                                                <select class="custom-select" id="meet_status" name="meet_status" required>

                                                    <option value="0" <?php if ($_meetingID['meet_status'] == 0) {
                                                                            echo 'selected';
                                                                        } ?>> รออนุมัติ </option>
                                                    <option value="1" <?php if ($_meetingID['meet_status'] == 1) {
                                                                            echo 'selected';
                                                                        } ?>> อนุมัติ </option>
                                                    <option value="2" <?php if ($_meetingID['meet_status'] == 2) {
                                                                            echo 'selected';
                                                                        } ?>> ยกเลิก</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-success">อัพเดท</button>
                                        <a href="<?= base_url('admin/meeting') ?>" class="btn btn-danger">ย้อนกลับ</a>
                                    </div>
                                </div>
                            </div>
                            <!-- end row -->
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>