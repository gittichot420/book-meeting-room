<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>เข้าสู่ระบบ</title>
    <!-- css -->
    <link rel="stylesheet" href="<?= base_url('assets/css/app.css'); ?>">
    <!-- icon menubar -->
    <!-- <link rel="icon" type="image/png" href="<?= base_url('./assets/img/logo.gif') ?>" /> -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/fontawesome-free/css/all.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/adminlte.min.css'); ?>">
</head>

<body class="bg-form">
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-2 col-lg-3"></div>
            <div class="col-md-8 col-lg-6">
                <div class="card bg-card" style="border-radius: 1rem;">
                    <div class="card-body text-black">
                        <div class="text-center p-4 mb-1 mt-n4">
                            <img src="<?= base_url('./assets/img/ICON-02.png'); ?>" alt="login form" class="img-fluid" />
                        </div>
                        <form action="<?= base_url('admin/login') ?>" method="post">
                            <div class="d-flex align-items-center">
                                <div class="col-sm-12 text-center">
                                    <span class="h2 fw-bold mb-0 font-weight-bold form-login">เข้าสู่ระบบ</span>
                                </div>
                            </div>
                            <div class="form-outline mt-4 mb-3 px-2">
                                <label class="form-label form-login" for="form2Example17">ชื่อผู้ใช้งาน</label>
                                <input name="username" type="text" id="form2Example17" class="form-control form-control" placeholder="ชื่อผู้ใช้งาน" required />
                                <!-- <input name="username" type="text" id="form2Example17" class="form-control form-control" onKeyPress="if(this.value.length==13) return false;" placeholder="ผู้ใช้งาน" required /> -->
                            </div>
                            <div class="form-outline px-2">
                                <label class="form-label form-login" for="form2Example27">รหัสผ่าน</label>
                                <input name="password" type="password" id="form2Example27" class="form-control form-control" placeholder="รหัสผ่าน" required />
                            </div>
                            <div class="pt-1 mt-5">
                                <button class="btn btn-primary btn-lg btn-block" type="submit">เข้าสู่ระบบ</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-lg-3"></div>
        </div>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>


<?php if ($this->session->flashdata('result') == 'success') {
    echo "<script>
        Swal.fire({
            icon: 'success',
            title: 'สำเร็จ',
            text: '" . $this->session->flashdata('message') . "', 
        })
    </script>";
} ?>
<?php if ($this->session->flashdata('result') == 'false') {
    echo "<script>
        Swal.fire({
            icon: 'error',
            title: 'ผิดพลาด',
            text: '" . $this->session->flashdata('message') . "',
        })
    </script>";
} ?>

</html>