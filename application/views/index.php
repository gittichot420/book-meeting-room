<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Meeting Rooms</title>
    <!-- css -->
    <link rel="stylesheet" href="<?= base_url('assets/css/app.css'); ?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('/assets/plugins/fontawesome-free/css/all.min.css'); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet"
        href="<?= base_url('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css'); ?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css'); ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/adminlte.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/style.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/badges.css'); ?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css'); ?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/daterangepicker/daterangepicker.css'); ?>">
    <!-- summernote -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/summernote/summernote-bs4.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css'); ?>">
    <link rel="stylesheet"
        href="<?= base_url('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css'); ?>">

    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <link rel="stylesheet" href="<?= base_url('lib/jquery.fancybox.css'); ?>" type="text/css" media="screen" />
    <link href='<?= base_url('fullcalendar/fullcalendar.css'); ?>' rel='stylesheet' />
    <link href='<?= base_url('fullcalendar/fullcalendar.print.css'); ?>' rel='stylesheet' media='print' />
    <script src="<?= base_url('lib/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- Custom Theme JavaScript -->
    <script src='<?= base_url('lib/moment.min.js'); ?>'></script>
    <script src='<?= base_url('fullcalendar/fullcalendar.min.js'); ?>'></script>
    <script src='<?= base_url('lib/lang/th.js'); ?>'></script>
    <script src="<?= base_url('lib/jquery.fancybox.pack.js'); ?>"></script>

    <!-- datetime picker -->
    <!-- <script src="https://unpkg.com/gijgo@1.9.14/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.14/css/gijgo.min.css" rel="stylesheet" type="text/css" /> -->

    <!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css"> -->

    <!-- 
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker-standalone.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script> -->

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <!-- css -->
    <style>
        sup {
            color: red;
        }

        .note-editable p {
            color: #000 !important;
        }

        /* .req {
            background-color: #ffcec5;
            border-color: #ffcec5;
        } */

        input[type="date"]::-webkit-datetime-edit,
        input[type="date"]::-webkit-inner-spin-button,
        input[type="date"]::-webkit-clear-button {
            color: #fff;
            position: relative;
        }

        input[type="date"]::-webkit-datetime-edit-year-field {
            position: absolute !important;
            padding: 2px;
            color: #000;
            left: 60px;
        }

        input[type="date"]::-webkit-datetime-edit-month-field {
            position: absolute !important;
            padding: 2px;
            color: #000;
            left: 30px;
        }


        input[type="date"]::-webkit-datetime-edit-day-field {
            position: absolute !important;
            color: #000;
            padding: 2px;
            left: 4px;
        }

        .border-radius-left {
            border-bottom-left-radius: 0 !important;
            border-top-left-radius: 0 !important;
        }

        .border-radius-right {
            border-bottom-right-radius: 0 !important;
            border-top-right-radius: 0 !important;
        }

        .bg-primary {
            background-color: #3f9bff !important;

        }

        .bg-warning:hover {
            background-color: #e1a900 !important;
        }

        .bg-success:hover {
            background-color: #00841e !important;
        }

        body {
            background-image: url('uploads/bg_home.png');
            background-repeat: no-repeat;
            background-size: 100% 100%;
            position: relative;
            background-attachment: fixed;
        }

        .img-fiuld {
            max-width: 70%;
        }

        #meetDetail p {
            color: #34495e !important;
        }
    </style>


    <!-- active path -->
    <script>
        $(document).ready(function() {
            var path = location.pathname.split('/').pop();
            if (path == '') {
                path = 'dashboard';
            }
            var target = $('nav a[id="' + path + '"]');
            target.addClass('active');
        });
    </script>

</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <!-- Image and text -->
    <nav class="navbar navbar-light bg-light"
        style="background: linear-gradient(90deg, #4F8BFF 15%, #CDFFFC 100%) !important;">
        <a href="" style="display: ruby;">
            <img src="<?= base_url('/uploads/logo.png') ?>" class="ml-3 img-fiuld" alt="">
        </a>
    </nav>
    <div class="content">
        <section class="content mt-5">
            <div class="container">
                <div class="mb-5">
                    <div class="row">
                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-4 col-md-6">
                            <div class="info-box py-3 px-3 bg-primary">
                                <span class="info-box-icon  px-5">
                                    <i class="fas fa-clipboard-list fa-2x"></i>
                                    <!-- <i class="fas fa-user-edit"></i> -->
                                </span>
                                <div class="info-box-content pl-3">
                                    <span class="info-box-text text-bold" style="font-size:20px">จองห้องประชุม</span>
                                    <span class="info-box-number p-0 m-0" style="font-size:20px">
                                        <?php if (isset($sum_meet_date)) {
                                            echo $sum_meet_date;
                                        } else {
                                            echo '0';
                                        } ?>
                                        <small>การจองวันนี้</small>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6">
                            <div class="info-box py-3 px-3 bg-warning">
                                <span class="info-box-icon px-5">
                                    <i class="far fa-calendar-alt fa-2x text-white"></i>
                                </span>
                                <div class="info-box-content pl-3">
                                    <span class="info-box-text text-bold text-white"
                                        style="font-size:20px">ห้องประชุมทั้งหมด</span>
                                    <span class="info-box-number p-0 m-0 text-white" style="font-size:20px">
                                        <?php if (isset($sum_room)) {
                                            echo $sum_room;
                                        } else {
                                            echo '0';
                                        } ?>
                                        <small>ห้อง</small>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6">
                            <div class="info-box py-3 px-3  bg-success">
                                <span class="info-box-icon px-5">
                                    <i class="fas fa-check fa-2x"></i>
                                </span>
                                <div class="info-box-content pl-3">
                                    <span class="info-box-text text-bold"
                                        style="font-size:20px">สถิติรายงานการจอง</span>
                                    <span class="info-box-number p-0 m-0" style="font-size:20px">
                                        <?php if (isset($sum_meet_month)) {
                                            echo $sum_meet_month;
                                        } else {
                                            echo '0';
                                        } ?>
                                        <small>การจองเดือนนี้</small>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-5">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs" id="bologna-list" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#description" role="tab" aria-controls="description"
                                    aria-selected="true"><i class="far fa-calendar-alt mr-1"></i> ปฏิทินห้องประชุม</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#meeting-room" role="tab" aria-controls="history"
                                    aria-selected="false">จองห้องประชุม</a>
                            </li>
                        </ul>
                    </div>
                    <style>
                        svg {
                            border: 1px solid #a0a0a0;
                            border-radius: 100%;
                        }
                    </style>
                    <div class="card-body">
                        <div class="tab-content mt-3">
                            <!-- Calendar -->
                            <div class="tab-pane active" id="description" role="tabpanel">
                                <div id='calendar'></div>
                                <div class="row mt-5">
                                    <?php foreach ($_getRooms as $rooms):  ?>
                                        <div class="col-6 col-md-3 col-lg-2 pb-3">
                                            <div class="card p-1" style="background-color: <?= $rooms['room_color_opacity']; ?>;">
                                                <small class="d-block text-lg-center">ห้องประชุม: <b style="font-weight: 500;"><?= $rooms['room_name']; ?> </b></small>
                                                <div class=" input-group">
                                                    <small>
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="10px" fill="<?= $rooms['room_color_opacity']; ?>">
                                                            <path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z" />
                                                        </svg>
                                                        รออนุมัติ
                                                    </small>
                                                    <small>
                                                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-lg-2" viewBox="0 0 512 512" width="10px" fill="<?= $rooms['room_color']; ?>">
                                                            <path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z" />
                                                        </svg>
                                                        อนุมัติแล้ว
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach;  ?>
                                </div>
                            </div>


                            <!-- Meeting Room -->
                            <div class="tab-pane" id="meeting-room" role="tabpanel" aria-labelledby="history-tab">
                                <form id="meet-room">
                                    <div class="form-outline mb-3 ">
                                        <label for="">ห้องประชุม <sup>*</sup></label>
                                        <select class="custom-select" id="room_id" name="room_id" required>
                                            <option value="" selected disabled> -โปรดเลือก- </option>
                                            <?php
                                            if (isset($_getRooms)) :
                                                foreach ($_getRooms as $rooms) :
                                            ?>
                                                    <option value="<?= $rooms['room_id'] ?>"><?= $rooms['room_name'] ?></option>
                                            <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-outline mb-3 ">
                                        <label for="">หัวข้อประชุม (เรื่อง) <sup>*</sup></label>
                                        <input type="text" id="meet_title" name="meet_title" class="form-control"
                                            value="" required />
                                    </div>
                                    <div class="form-outline mb-3 ">
                                        <label for="">รายละเอียด</label>
                                        <textarea type="text" id="meet_detail" name="meet_detail"
                                            class="form-control detail" value=""></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="form-outline mb-3 col-12 col-lg-3">
                                            <label for="">จำนวนผู้เข้าร่วมประชุม <sup>*</sup></label>
                                            <input type="number" id="meet_unit" name="meet_unit" class="form-control"
                                                value="" required />
                                        </div>
                                        <div class="form-outline mb-3 col-12 col-lg-3">
                                            <label for="">ชื่อผู้จองห้องประชุม <sup>*</sup></label>
                                            <input type="text" id="meet_name" name="meet_name" class="form-control"
                                                value="" required />
                                        </div>
                                        <div class="form-outline mb-3 col-12 col-lg-3">
                                            <label for="">สังกัดผู้จองห้องประชุม <sup>*</sup></label>
                                            <input type="text" id="meet_position" name="meet_position"
                                                class="form-control" value="" required />
                                        </div>
                                        <div class="form-outline mb-3 col-12 col-lg-3">
                                            <label for="">เบอร์โทรติดต่อ <sup>*</sup></label>
                                            <input type="number" id="meet_tell" name="meet_tell" class="form-control"
                                                value="" required />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-outline mb-3 col-12 col-lg-3">
                                            <label for="">วันเริ่มประชุม <sup>*</sup></label>
                                            <input type="date" id="meet_date_start" name="meet_date_start"
                                                class="form-control" required>
                                        </div>
                                        <div class="form-outline mb-3 col-12 col-lg-3">
                                            <label for="">เวลาเริ่มประชุม <sup>*</sup></label>
                                            <div class="input-group-prepend">
                                                <input type="text" id="meet_time_start" name="meet_time_start"
                                                    class="form-control timepicker border-radius-right" placeholder="00:00" required>
                                                <span class="input-group-text  border-radius-left ">น.</span>
                                            </div>
                                        </div>
                                        <div class="form-outline mb-3 col-12 col-lg-3">
                                            <label for="">วันสิ้นสุดประชุม <sup>*</sup></label>
                                            <input type="date" id="meet_date_end" name="meet_date_end"
                                                class="form-control" required>
                                        </div>
                                        <div class="form-outline mb-3 col-12 col-lg-3">
                                            <label for="">เวลาสิ้นสุดประชุม <sup>*</sup></label>
                                            <div class="input-group-prepend">
                                                <input type="text" id="meet_time_end" name="meet_time_end"
                                                    class="form-control timepicker border-radius-right" placeholder="00:00" required>
                                                <span class="input-group-text  border-radius-left ">น.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mt-5 text-center">
                                            <button id="save-meet" type="submit"
                                                class="btn btn-success mr-2">จองห้องประชุม</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card-footer mt-5">
                        <div class="row">
                            <div class="col-3">1</div>
                            <div class="col-3">2</div>
                            <div class="col-3">3</div>
                            <div class="col-3">4</div>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>
        <div id="calendarModal" class="modal fade" style="z-index: 1400;">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #f1f1f1;">
                        <h5 id="modalTitle" class="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td width="40%"><b>วัน-เวลาเริ่ม: </b><br>
                                        <b>วัน-เวลาสิ้นสุด:</b>
                                    </td>
                                    <td id="meetingDate"></td>
                                </tr>
                                <tr>
                                    <td><b>ห้องประชุม:</b> </td>
                                    <td id="roomName"></td>
                                </tr>
                                <tr>
                                    <td><b>ข้อมูลผู้จองห้องประชุม:</b> </td>
                                    <td id="meetName"></td>
                                </tr>
                                <tr>
                                    <td><b>จำนวนผู้เข้าร่วมประชุม:</b> </td>
                                    <td id="meetUnit"></td>
                                </tr>
                                <tr>
                                    <td><b>รายละเอียด:</b> </td>
                                    <td id="meetDetail"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
    </div>


    <!-- Sweetalert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.15.6/dist/sweetalert2.all.min.js"></script>
    <script>
        $('#save-meet').on('click', function(event) {
            var roomId = $('#room_id').val();
            var meet_title = $('#meet_title').val();
            var meet_unit = $('#meet_unit').val();
            var meet_name = $('#meet_name').val();
            var meet_position = $('#meet_position').val();
            var meet_detail = $('#meet_detail').val();
            var meet_tell = $('#meet_tell').val();
            var meet_date_start = $('#meet_date_start').val();
            var meet_time_start = $('#meet_time_start').val();
            var meet_date_end = $('#meet_date_end').val();
            var meet_time_end = $('#meet_time_end').val();

            if (roomId == '' || meet_title.trim().length < 1 || meet_unit.trim().length < 1 || meet_name.trim()
                .length < 1 || meet_position.trim().length < 1 ||
                meet_tell.trim().length < 1 || meet_date_start.trim().length < 1 || meet_time_start.trim().length <
                1 || meet_date_end.trim().length < 1 || meet_time_end.trim().length < 1) {
                Swal.fire({
                    icon: "error",
                    title: "ผิดพลาด",
                    text: "กรุณากรอกข้อมูลให้ครบทุกช่อง",
                });
            } else {
                var arrData = $('#meet-room').serialize();
                $.ajax({
                    url: "Frontend/save_meetimg",
                    type: 'post',
                    dataType: "json",
                    data: arrData,
                    success: (res) => {
                        Swal.fire({
                            title: "Good job",
                            text: "You clicked the button!",
                            icon: "success",
                            timer: 5000,
                        }).then((resule) => {
                            if (result.isConfirmed) {
                                location = "<?= base_url() ?>"
                            }
                        });
                    },
                });
            }
        });

        $('.timepicker').timepicker({
            timeFormat: 'HH:mm',
            interval: 30,
            minTime: '8',
            maxTime: '18',
            dynamic: false,
            dropdown: true,
            scrollbar: true
        });

        $('#bologna-list a').on('click', function(e) {
            e.preventDefault()
            $(this).tab('show')
        })


        var events = <?= json_encode($data) ?>;
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            titleFormat: {
                month: 'MMMM YYYY'
            },
            viewRender: function(view, element) {
                setTimeout(function() {
                    var strDate = $.trim($(".fc-center").find("h2").text());
                    var arrDate = strDate.split(" ");
                    var lengthArr = arrDate.length;
                    var newstrDate = "";
                    for (var i = 0; i < lengthArr; i++) {
                        if (lengthArr - 1 == i || parseInt(arrDate[i]) > 1000) {
                            var yearBuddha = parseInt(arrDate[i]) + 543;
                            newstrDate += yearBuddha;
                        } else {
                            newstrDate += arrDate[i] + " ";
                        }
                    }
                    $(".fc-center").find("h2").text(newstrDate);
                }, 5);
            },
            height: 'parent',
            events: events,
            displayEventEnd: true,
            eventLimit: true,
            defaultDate: new Date(),
            timezone: 'Asia/Bangkok',
            loading: function(bool) {
                $('#loading').toggle(bool);
            },
            height: 650,
            eventClick: function(events) {
                var meetStatus = '';
                if (events.status == 1) {
                    meetStatus =
                        '  <sup><span class="badge badge-pill" style="color: white;font-size:12px;background-color: ' + events.color + ';">อนุมัติแล้ว</span></sup>';
                }
                if (events.status == 0) {
                    meetStatus =
                        '  <sup><span class="badge badge-pill" style="color: white;font-size:12px;background-color: ' + events.color + ';">รออนุมัติ</span></sup>';
                }

                $('#modalTitle').html(events.title + meetStatus);
                var fdate = events.date_start + ' ' + events.ftime_start + ' น. <br>' + events.date_end + ' ' +
                    events.ftime_end + ' น.';
                $('#meetingDate').html(fdate);
                $('#meetName').html(events.name);
                $('#meetUnit').html(events.unit + ' คน');
                $('#roomName').html(events.room);
                $('#meetDetail').html(events.detail);
                $('#calendarModal').modal();
            },
            editable: true,
            dayMaxEvents: true,
        });
    </script>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>

    <!-- jQuery -->
    <script src="<?= base_url('lib/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?= base_url('assets/plugins/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <!-- ChartJS -->
    <script src="<?= base_url('assets/plugins/chart.js/Chart.min.js'); ?> "></script>
    <!-- jQuery Knob Chart -->
    <script src="<?= base_url('assets/plugins/jquery-knob/jquery.knob.min.js'); ?> "></script>
    <!-- daterangepicker -->
    <script src="<?= base_url('assets/plugins/moment/moment.min.js'); ?> "></script>
    <script src="<?= base_url('assets/plugins/daterangepicker/daterangepicker.js'); ?> "></script>

    <!-- Tempusdominus Bootstrap 4 -->
    <script src="<?= base_url('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js'); ?> ">
    </script>
    <!-- Summernote -->
    <script src="<?= base_url('assets/plugins/summernote/summernote-bs4.min.js'); ?> "></script>
    <!-- overlayScrollbars -->
    <script src="<?= base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js'); ?> "></script>
    <!-- AdminLTE for demo purposes -->
    <!-- <script src="<?= base_url('assets/dist/js/demo.js'); ?>"></script> -->

    <script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/jszip/jszip.min.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/pdfmake/pdfmake.min.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/pdfmake/vfs_fonts.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.html5.min.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.print.min.js'); ?>"></script>
    <script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.colVis.min.js'); ?>"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>

    <script src="<?= base_url('assets/dist/js/adminlte.min.js'); ?>"></script>

    <!-- Modal -->
    <!-- Modal -->
    <script src="<?= base_url('assets/js/modal.js'); ?>"></script>


    <script>
        $('.detail').summernote({
            placeholder: 'รายละเอียดเพิ่มเติม',
            height: '400px',
        });
    </script>

</body>

</html>