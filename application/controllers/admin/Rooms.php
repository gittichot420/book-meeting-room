<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rooms extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (empty($this->session->userdata('users'))) {
            redirect(base_url());
        }
        $this->load->model('admin/rooms_model', 'rooms');
    }

    public function index()
    {
        $this->data['arrRooms'] = $this->rooms->_getRooms();

        $this->load->view('admin/theme/header');
        $this->load->view('admin/rooms/index', $this->data);
        $this->load->view('admin/theme/footer');
    }
    /* Convert hexdec color string to rgb(a) string */

    // function hex2rgb($hex, $opacity = false)
    // {
    //     $hex = str_replace("#", "", $hex);

    //     if (strlen($hex) == 3) {
    //         $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
    //         $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
    //         $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
    //     } else {
    //         $r = hexdec(substr($hex, 0, 2));
    //         $g = hexdec(substr($hex, 2, 2));
    //         $b = hexdec(substr($hex, 4, 2));
    //     }
    //     $rgb = array($r, $g, $b);

    //     if ($opacity) {
    //         if (abs($opacity)) {
    //             // $opacity = 1.0;
    //             $output = 'rgba(' . implode(",", $rgb) . ',' . $opacity . ')';
    //         }
    //     } else {
    //         $output = 'rgb(' . implode(",", $rgb) . ')';
    //     }

    //     //Return rgb(a) color string
    //     return $output;
    // }

    public function add()
    {
        if ($this->input->post()) {
            $room_name = $this->input->post('room_name');
            $room_detail = $this->input->post('room_detail');
            $room_color = $this->input->post('room_color');
            $active = 1;

            $_formdata = [
                'room_name' => $room_name,
                'room_detail' => $room_detail,
                'room_color' => $room_color,
                'room_color_opacity' => $room_color . '66',
                'room_active' => $active,
            ];

            if ($_FILES["room_image"] != NULL) {
                $_formdata['room_image'] = $this->upload_file_service('room_image', '1920', '1080', './uploads/img/');
            }

            $result = $this->rooms->add_rooms($_formdata);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'เพิ่มข้อมูลห้องประชุมสำเร็จ.');
                redirect(base_url('admin/rooms'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'เพิ่มข้อมูลห้องประชุมไม่สำเร็จ.');
                redirect(base_url('admin/rooms'));
            }
        }
        $this->data['menu_page'] = 'rooms';

        $this->load->view('admin/theme/header', $this->data);
        $this->load->view('admin/rooms/add', $this->data);
        $this->load->view('admin/theme/footer');
    }

    public function edit($id = null)
    {
        if (!$id) {
            redirect(base_url('admin/users'));
        }
        $this->data['_RoomsID'] = $this->rooms->_getRoomsID($id);

        if ($this->input->post()) {

            $room_name = $this->input->post('room_name');
            $room_detail = $this->input->post('room_detail');
            $room_color = $this->input->post('room_color');
            $active = $this->input->post('active');
            $getImage = $this->input->post('get_img');

            $_formdata = [
                'room_name' => $room_name,
                'room_detail' => $room_detail,
                'room_color' => $room_color,
                'room_color_opacity' => $room_color . '66',
                'room_active' => $active,
            ];

            if (!empty($_FILES["room_image"])) {
                $uploadImg = $this->upload_file_service('room_image', '1920', '1080', './uploads/img/');
                if ($uploadImg) {
                    $_formdata['room_image'] = $uploadImg;
                } else {
                    $_formdata['room_image'] = $getImage;
                }
            }

            $result = $this->rooms->update_rooms($id, $_formdata);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'แก้ไขข้อมูลห้องประชุมสำเร็จ.');
                redirect(base_url('admin/rooms'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'แก้ไขข้อมูลห้องประชุมไม่สำเร็จ.');
                redirect(base_url('admin/rooms/' . $id));
            }
        }

        $this->data['menu_page'] = 'rooms';


        $this->load->view('admin/theme/header', $this->data);
        $this->load->view('admin/rooms/edit', $this->data);
        $this->load->view('admin/theme/footer');
    }

    public function del($id = null)
    {
        if ($id != null) {
            $result = $this->rooms->del_rooms($id);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลห้องประชุมสำเร็จ.');
                redirect(base_url('admin/rooms'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลห้องประชุมไม่สำเร็จ.');
                redirect(base_url('admin/rooms'));
            }
        } else {
            redirect(base_url('admin/rooms'));
        }
    }

    function upload_file_service($pic, $w, $h, $path)
    {
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gift|png|jpg|jpeg';
        $config['max_size'] = 100000;
        $config['max_width'] = 4800;
        $config['max_height'] = 3500;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload($pic)) {
            $data = $this->upload->data();
            //ชื่อรูป
            $name_pic =  'room_' . gen_namepic(5);
            rename($data['full_path'], $data['file_path'] . $name_pic . $data['file_ext']);
            // resize
            //$width = $this->upload->image_width;
            //$height = $this->upload->image_height;
            $config['image_library'] = 'gd2';
            $config['source_image'] = $data['file_path'] . $name_pic . $data['file_ext'];
            $config['width'] = $w;
            $config['height'] = $h;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $picname = $name_pic . $data['file_ext']; // ชื่อรูปภาพ
        } else {
            $picname = '';
            echo $this->upload->display_errors();
        }
        return $picname;
    }
}
