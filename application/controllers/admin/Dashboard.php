<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (empty($this->session->userdata('users'))) {
            redirect(base_url());
        }
        $this->load->model('meeting_model', 'meeting');
        $this->load->model('admin/rooms_model', 'rooms');
    }

    public function index()
    {
        $meetimg_room = $this->db->join('rooms', 'rooms.room_id = meeting_room.room_id')->where('meet_status != 2')->get("meeting_room")->result_array();
        if (!empty($meetimg_room)) {
            foreach ($meetimg_room as $key => $value) {

                if ($value['meet_status'] == 1) {
                    if (isset($value['room_color'])) {
                        $color = $value['room_color'];
                    } else {
                        $color = '#28a745';
                    }
                }
                if ($value['meet_status'] == 0) {
                    if (isset($value['room_color_opacity'])) {
                        $color = $value['room_color_opacity'];
                    } else {
                        $color = '#FF9900';
                    }
                }
                $start = date("d-m-Y", strtotime($value['meet_date_start']));
                $end =  date("d-m-Y", strtotime($value['meet_date_end']));
                $tstart =  date("H:i", strtotime($value['meet_time_start']));
                $tend =  date("H:i", strtotime($value['meet_time_end']));;
                $this->data['data'][$key]['id'] = $value['meet_id'];
                $this->data['data'][$key]['title'] = $value['meet_title'];
                $this->data['data'][$key]['start'] =  $value['meet_date_start'] . ' ' . $value['meet_time_start'];
                $this->data['data'][$key]['end'] = $value['meet_date_end'] . ' ' . $value['meet_time_end'];
                $this->data['data'][$key]['room'] =  $value['room_name'];
                $this->data['data'][$key]['time_start'] = $value['meet_time_start'];
                $this->data['data'][$key]['time_end'] = $value['meet_time_end'];
                $this->data['data'][$key]['date_start'] = $start;
                $this->data['data'][$key]['date_end'] = $end;
                $this->data['data'][$key]['ftime_start'] = $tstart;
                $this->data['data'][$key]['ftime_end'] = $tend;
                $this->data['data'][$key]['status'] = $value['meet_status'];
                $this->data['data'][$key]['unit'] = $value['meet_unit'];
                if ($value['meet_detail'] == '') {
                    $this->data['data'][$key]['detail'] = '-';
                } else {
                    $this->data['data'][$key]['detail'] = $value['meet_detail'];
                }
                $this->data['data'][$key]['name'] = 'ชื่อ: ' . $value['meet_name'] . '<br> เบอร์: ' . $value['meet_tell'] . '<br>  สังกัด: ' . $value['meet_position'];
                $this->data['data'][$key]['color'] = $color;
            }
        } else {
            $this->data['data'] = null;
        }

        $this->data['sum_meet_date'] =  $this->meeting->sum_date(date("Y-m-d"));
        $this->data['sum_room'] =   $this->rooms->sum_room();
        $this->data['sum_meet_month'] =   $this->meeting->sum_month(date("Y-m"));


        $month = $this->input->get('month');
        if (empty($month)) {
            $month = date('Y-m');
        }
        $this->data['menu_page'] = 'dashboard';
        $this->data['month'] = $month;
        $this->data['arrRooms'] = $this->rooms->_getRooms();


        $this->load->view('admin/theme/header', $this->data);
        $this->load->view('admin/index', $this->data);
        $this->load->view('admin/theme/footer');
    }
}
