<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (empty($this->session->userdata('users'))) {
            redirect(base_url());
        }
        $this->load->model('admin/users_model', 'users');
    }

    public function password_hash($password)
    {
        return md5($password);
    }

    public function index()
    {
        $this->data['arrUsers'] = $this->users->_getUsers();

        $this->load->view('admin/theme/header');
        $this->load->view('admin/users/index', $this->data);
        $this->load->view('admin/theme/footer');
    }

    public function add()
    {
        if ($this->input->post()) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $prefix = $this->input->post('prefix');
            $first_name = $this->input->post('fname');
            $last_name = $this->input->post('lname');
            $position = $this->input->post('position');
            $active = 1;
            $created_at = date('Y-m-d H:i:s');


            $pass_hash = $this->password_hash($password);
            $_formdata = [
                'u_user' => $username,
                'u_pass' => $pass_hash,
                'u_realpass' => $password,

                'u_prefix' => $prefix,
                'u_fname' => $first_name,
                'u_lname' => $last_name,
                'u_position' => $position,
                'u_active' => $active,
                'created_at' => $created_at,
            ];

            $result = $this->users->add_users($_formdata);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'เพิ่มข้อมูลสมาชิกสำเร็จ.');
                redirect(base_url('admin/users'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'เพิ่มข้อมูลสมาชิกไม่สำเร็จ.');
                redirect(base_url('admin/users'));
            }
        }
        $this->data['menu_page'] = 'users';

        $this->load->view('admin/theme/header', $this->data);
        $this->load->view('admin/users/add', $this->data);
        $this->load->view('admin/theme/footer');
    }

    public function edit($id = null)
    {
        if (!$id) {
            redirect(base_url('admin/users'));
        }

        if ($this->input->get()) {

            $username = $this->input->get('username');

            $prefix = $this->input->get('prefix');
            $first_name = $this->input->get('fname');
            $last_name = $this->input->get('lname');
            $position = $this->input->get('position');
            $active = $this->input->get('active');

            if (!empty($this->input->get('password'))) {
                $password = $this->input->get('password');
                $pass_hash = $this->password_hash($password);

                $_formdata = [
                    'u_user' => $username,
                    'u_pass' => $pass_hash,
                    'u_realpass' => $password,

                    'u_prefix' => $prefix,
                    'u_fname' => $first_name,
                    'u_lname' => $last_name,
                    'u_position' => $position,
                    'u_active' => $active,
                ];
            } else {
                $_formdata = [
                    'u_user' => $username,

                    'u_prefix' => $prefix,
                    'u_fname' => $first_name,
                    'u_lname' => $last_name,
                    'u_position' => $position,
                    'u_active' => $active,
                ];
            }


            $result = $this->users->update_users($id, $_formdata);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'แก้ไขข้อมูลสมาชิกสำเร็จ.');
                redirect(base_url('admin/users'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'แก้ไขข้อมูลสมาชิกไม่สำเร็จ.');
                redirect(base_url('admin/users/' . $id));
            }
        }

        $this->data['menu_page'] = 'users';
        $this->data['_userID'] = $this->users->_getUserID($id);


        $this->load->view('admin/theme/header', $this->data);
        $this->load->view('admin/users/edit', $this->data);
        $this->load->view('admin/theme/footer');
    }

    public function del($id = null)
    {
        if ($id != null) {
            $result = $this->users->del_users($id);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลผู้ใช้งานสำเร็จ.');
                redirect(base_url('admin/users'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'ลบข้อมูลผู้ใช้งานไม่สำเร็จ.');
                redirect(base_url('admin/users'));
            }
        } else {
            redirect(base_url('admin/users'));
        }
    }
}
