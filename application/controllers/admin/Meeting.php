<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Meeting extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (empty($this->session->userdata('users'))) {
            redirect(base_url());
        }
        $this->load->model('meeting_model', 'meeting');
        $this->load->model('admin/rooms_model', 'rooms');
    }

    public function index()
    {
        $this->data['menu_page'] = 'meeting';
        $this->data['arrMeeting'] = $this->meeting->getMeeting();

        $this->load->view('admin/theme/header', $this->data);
        $this->load->view('admin/meeting/index', $this->data);
        $this->load->view('admin/theme/footer');
    }

    public function edit($id = null)
    {
        $this->data['menu_page'] = 'meeting';
        $this->data['_getRooms'] = $this->rooms->_getRooms();
        $this->data['_meetingID'] = $this->meeting->_getMeetingID($id);
        $this->data['id'] = $id;

        if ($this->input->get()) {

            echo '<pre>';
            print_r($this->input->get());
            echo '</pre>';

            $room_id = $this->input->get('room_id');
            $meet_title = $this->input->get('meet_title');
            $meet_detail = $this->input->get('meet_detail');
            $meet_unit = $this->input->get('meet_unit');
            $meet_name = $this->input->get('meet_name');
            $meet_position = $this->input->get('meet_position');
            $meet_tell = $this->input->get('meet_tell');
            $meet_date_start = $this->input->get('meet_date_start');
            $meet_time_start = $this->input->get('meet_time_start');
            $meet_date_end = $this->input->get('meet_date_end');
            $meet_time_end = $this->input->get('meet_time_end');
            $meet_status = $this->input->get('meet_status');

            $_data = [
                'room_id' =>  $room_id,
                'meet_title' =>  $meet_title,
                'meet_detail' =>  $meet_detail,
                'meet_unit' =>  $meet_unit,
                'meet_name' =>  $meet_name,
                'meet_position' =>  $meet_position,
                'meet_tell' =>  $meet_tell,
                'meet_date_start' =>  $meet_date_start,
                'meet_time_start' =>  $meet_time_start,
                'meet_date_end' =>  $meet_date_end,
                'meet_time_end' =>  $meet_time_end,
                'meet_status' =>  $meet_status,
            ];

            $result =  $this->meeting->update_meetimg($id, $_data);
            if ($result == 'success') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'แก้ไขรายการจองสำเร็จ.');
                redirect(base_url('admin/meeting'));
            } else if ($result == 'false') {
                $this->session->set_flashdata('result', $result);
                $this->session->set_flashdata('message', 'แก้ไขรายการจองไม่สำเร็จ.');
                redirect(base_url('admin/meeting'));
            }
        } else {
            $this->load->view('admin/theme/header', $this->data);
            $this->load->view('admin/meeting/edit', $this->data);
            $this->load->view('admin/theme/footer');
        }
    }

    public function ajax_update_meetimg()
    {
        $id = $this->input->post('meet_id');
        $_data = [
            'meet_status' => 1,
        ];

        $this->meeting->update_meetimg($id, $_data);
        echo json_encode(true);
    }
}
