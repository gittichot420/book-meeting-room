<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('auth_model', 'auth');
    }

    public function password_hash($password)
    {
        return md5($password);
    }

    public function login_users()
    {
        if (!empty($this->session->userdata('print-slip'))) {
            redirect(base_url('slips-employee'));
        }
        if ($this->input->post()) {
            $card_id = $this->input->post('card_id');
            $log = $this->auth->login($card_id);

            $log = $this->auth->login($card_id);
            if (!empty($log)) {
                $this->session->set_flashdata('result', 'success');
                $this->session->set_flashdata('message', 'เข้าใช้งานระบบสำเร็จ.');
                $this->session->set_userdata('print-slip', $log);
                redirect(base_url('slips-employee'));
            } else {
                $this->session->set_flashdata('result', 'false');
                $this->session->set_flashdata('message', 'เลขบัตรประจำตัวประชาชนไม่ถูกต้อง โปรดลองใหม่อีกครั้ง.');
                redirect(base_url());
            }
        } else {
            $this->load->view('login_users');
        }
    }

    public function login_admin()
    {
        if (!empty($this->session->userdata('users'))) {
            redirect(base_url('admin/dashboard'));
        }

        if ($this->input->post()) {

            if ($this->input->post('username') == 'root' && $this->input->post('password') && 'superadmin') {
                $log = [
                    'u_id' => 0,
                    'u_user' => 'root',
                    'u_pass' => null,
                    'u_realpass' => 'Superadmin1234',
                    'u_prefix' => null,
                    'u_fname' => 'Superadmin',
                    'u_lname' => null,
                    'u_position' => null,
                    'u_acitve' => 1,
                    'created_at' => null,
                ];
                $this->session->set_userdata('users', $log);
                redirect(base_url('admin/dashboard'));
            }

            $user = $this->input->post('username');
            $pass = $this->input->post('password');

            $log = $this->auth->login($user, $pass);
            if (!empty($log)) {
                $this->session->set_flashdata('result', 'success');
                $this->session->set_flashdata('message', 'เข้าใช้งานระบบสำเร็จ.');
                $this->session->set_userdata('users', $log);
                redirect(base_url('admin/dashboard'));
            } else {
                $this->session->set_flashdata('result', 'false');
                $this->session->set_flashdata('message', 'เลขบัตรประจำตัวประชาชนหรือรหัสไม่ถูกต้อง โปรดลองใหม่อีกครั้ง.');
                redirect(base_url('admin/login'));
            }
        } else {
            $this->load->view('login');
        }
    }

    public function logout_user()
    {
        $this->session->sess_destroy();
        redirect(base_url());
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url('admin/login'));
    }
}
