<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Showevents extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (empty($this->session->userdata('users'))) {
            redirect(base_url());
        }
        $this->load->model('meeting_model', 'meeting');
    }

    public function index()
    {
        $meet_id = $this->input->get('id');
        $this->data['events'] = $this->meeting->_getMeetingID($meet_id);

        $this->load->view('admin/calendar/showeventsdata', $this->data);
    }
}
