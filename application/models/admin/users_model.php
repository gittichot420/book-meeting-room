<?php

defined('BASEPATH') or exit('No direct script access allowed');

class users_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function _getUsers()
    {
        return $this->db->order_by('u_fname')->get('users')->result_array();
    }

    public function _getUserID($id)
    {
        return $this->db->where('u_id', $id)->get('users')->row_array();
    }

    public function add_users($data_insert = null)
    {
        $result = $this->db->insert('users', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function update_users($m_id = null, $data_insert = null)
    {
        $result = $this->db->where('u_id', $m_id)->update('users', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function del_users($m_id = null)
    {
        $result = $this->db->where('u_id', $m_id)->delete('users');

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }
}
