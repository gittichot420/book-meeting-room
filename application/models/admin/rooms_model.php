<?php

defined('BASEPATH') or exit('No direct script access allowed');

class rooms_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function _getRooms()
    {
        return $this->db->get('rooms')->result_array();
    }

    public function sum_room()
    {
        $res = $this->db->where('room_active', '1')->select('COUNT(room_id) AS sum_room')->get('rooms')->row_array();
        return $res['sum_room'];
    }

    public function _getRoomsID($id)
    {
        return $this->db->where('room_id', $id)->get('rooms')->row_array();
    }

    public function add_rooms($data_insert = null)
    {
        $result = $this->db->insert('rooms', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function update_rooms($id = null, $_data = null)
    {
        $result = $this->db->where('room_id', $id)->update('rooms', $_data);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function del_rooms($m_id = null)
    {
        $result = $this->db->where('room_id', $m_id)->delete('rooms');

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }
}
