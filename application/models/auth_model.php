<?php

defined('BASEPATH') or exit('No direct script access allowed');

class auth_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function login($user = null, $pass = null)
    {
        $where = ['u_user' => $user, 'u_realpass' => $pass, 'u_active' => 1];
        return $this->db->where($where)->get('users')->row_array();
    }

    public function register($data_insert = null)
    {
        $result = $this->db->insert('members', $data_insert);

        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }

    public function check_user($username = null)
    {
        return $this->db->where('u_username', $username)->get('users')->row_array();
    }
    public function get_user()
    {
        return $this->db->limit(1)->get('members')->result_array();
    }
}