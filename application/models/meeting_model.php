<?php

defined('BASEPATH') or exit('No direct script access allowed');

class meeting_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getMeeting()
    {
        return $this->db->join('rooms', 'rooms.room_id = meeting_room.room_id')->get('meeting_room')->result_array();
    }

    public function sum_date($date_now = null)
    {
        $res = $this->db->where('meet_date_start', $date_now)->select('COUNT(meet_id) AS sum_date_now')->get('meeting_room')->row_array();
        return $res['sum_date_now'];
    }

    public function sum_month($month = null)
    {
        $res = $this->db->where('meet_date_start LIKE', $month . '%')->select('count(meet_id) as sum_month ')->get('meeting_room')->row_array();
        return $res['sum_month'];
    }


    public function _getMeetingID($id = null)
    {
        return $this->db->where('meet_id', $id)->join('rooms', 'rooms.room_id = meeting_room.room_id')->get('meeting_room')->row_array();
    }

    public function save_meetimg($data_insert = null)
    {
        $this->db->insert('meeting_room', $data_insert);
    }

    public function ajax_update_meetimg()
    {
        $meet_id = $this->input->post('meet_id');
        $data = array(
            'meet_status' => 1,
        );
        $this->db->where('meet_id', $meet_id);
        $this->db->update('meeting_room', $data);
    }


    public function update_meetimg($id = null, $_data = null)
    {
        $result =  $this->db->where('meet_id', $id)->update('meeting_room', $_data);
        if ($result) {
            return 'success';
        } else {
            return 'false';
        }
    }
}
